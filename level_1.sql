create or replace FUNCTION DURATION_TO_STRING(DURATION INT)
returns varchar(16) as
$$
declare
	mins int;
	sec int;
begin
	if duration > 0 then
		mins := DURATION / 60;
		sec := DURATION % 60;
		return mins::text || ':' || to_char(sec, 'FM09');
	else
		return '0:00';
	end if;
end;
$$ LANGUAGE plpgsql;

create or replace function add_artist(name varchar(64), birthdate date)
returns boolean as
$$
begin
insert into artist values (default, name, birthdate);
return true;
exception when others then
return false;
end;
$$ LANGUAGE plpgsql;

create or replace function add_album(name varchar(64))
returns boolean as
$$
begin
insert into album values (default, name, 1);
return true;
exception when others then
return false;
end;
$$ language plpgsql;

create or replace function add_song(title varchar(64), album varchar(64), artist varchar(64),
					track INT, DURATION INT)
returns boolean as
$$
declare
test int;
begin
if artist not in (select artist_name from artist) or album not in (select album_name from album ) then 
 return false;
else
insert into song values (default, title, artist, duration);
end if;
select song_id into test from song where song.songs_title = add_song.title and song.artist = add_song.artist; 
insert into album_song values (default, test,  album, track);
return true;

exception when others then
 return false;
end;
$$ language plpgsql;

create or replace function add_song_to_album(title varchar(64), album varchar(64),
						artist varchar(64), track int)
returns boolean as
$$
declare
test2 int;
begin
if title not in (select songs_title from song) and album not in (select album_name from album)
		or artist not in (select artist_name from artist) then
return false;
elsif exists (select * from album_song inner join song on song.song_id = album_song.songs_id
where album_song.title = add_song_to_album.album
and songs_title = add_song_to_album.title
and add_song_to_album.artist = song.artist) then
return false;
end if;
select song.song_id into test2 from song where song.songs_title = add_song_to_album.title
and song.artist = add_song_to_album.artist;
if test2 is null then
return false;
else
insert into album_song values (default, test2, album, track);
return true;
end if;
exception when others then
 return false;
end;
$$ language plpgsql;

create or replace view view_artists as
select artist_name as artist, birthdate, count(*) as songs from artist inner join song
on artist.artist_name = song.artist group by artist_name, birthdate order by artist_name;

create or replace view view_albums as
select album_name as album, count(song.song_id) as songs,
duration_to_string(cast(sum(song.duration) as integer)) as duration
from album left join album_song on album.album_name = album_song.title left join song
on album_song.songs_id = song.song_id group by album_name order by album_name;

create or replace view view_songs as
select songs_title as music, artist, duration_to_string(duration) as duration from song
group by songs_title, artist, duration order by songs_title, artist;
