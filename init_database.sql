DROP TABLE IF EXISTS album CASCADE;
DROP TABLE IF EXISTS song CASCADE;
DROP TABLE IF EXISTS artist CASCADE;
DROP TABLE IF EXISTS album_song CASCADE;
DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS stock CASCADE;
DROP TABLE IF EXISTS rent_album CASCADE;


CREATE TABLE album
(
	album_id serial NOT NULL,
	album_name VARCHAR(64) NOT NULL unique,
	album_stock INT NOT NULL, check ( album_stock >= 0 ),
	primary key(album_id)
);

CREATE TABLE artist
(
	artist_id serial not null,
	artist_name varchar(64) not null unique,
	birthdate date NOT NULL,
	primary key(artist_id)
);

CREATE TABLE song
(
	song_id serial not null,
	songs_title varchar(64) not null,
	artist varchar(64) not null,
	duration int not null unique, check (duration >= 0),
	primary key(song_id)
);


CREATE TABLE album_song
(
	album_song_id serial not null,
	songs_id INT NOT NULL,
	title varchar(64) not null,
	track INT NOT NULL unique, check( track > 0),
	primary key (album_song_id),
	UNIQUE (title, track),
	foreign key(songs_id) references song(song_id),
	foreign key(title) references album(album_name)
);

CREATE TABLE customer
(
	customer_id SERIAL NOT NULL,
	name varchar(64) NOT NULL UNIQUE,
	mail varchar(64) NOT NULL UNIQUE,
	current_rent INT NOT NULL DEFAULT 0,
	total_rent INT NOT NULL DEFAULT 0,
	primary key(customer_id)
);

CREATE TABLE stock
(
	stock_id SERIAL NOT NULL,
	mail varchar(64) not null unique,
	album_stock_name VARCHAR(64) NOT NULL,
	rent_date DATE not null,
	return_date DATE NULL,
	primary key(stock_id),
	foreign key (album_stock_name) references album(album_name)
);

CREATE TABLE RENT_ALBUM
(
	id serial not null,
	album_name varchar(64) not null,
	mail varchar(64) not null,
	rent_date DATE NOT NULL,
	return_date DATE NULL,
	fee int not null, check (fee > 0),
	primary key(id)
);
