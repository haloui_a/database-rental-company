create or replace function add_customer(name varchar(64), EMAIL varchar(64))
returns boolean as
$$
begin
insert into customer values (default, name, email, default, default);
return true;
exception when others then
return false;
end;
$$ language plpgsql;

create or replace function add_stock(album varchar(64), N INT)
returns boolean as
$$
begin
if add_stock.album in (select album_name from album) and  N > 0 then
UPDATE album
set album_stock = album_stock + N where add_stock.album = album_name;
return true;
end if;
return false;
exception when others then
 return false;
end;
$$ language plpgsql;

create or replace function rent_album(customer_mail varchar(64), album varchar(64), rent_date date)
returns boolean as
$$
begin
if customer_mail not in (select mail from customer)
   and rent_album.album not in( select album_name from album)
   and rent_album.rent_date in ( select stock.rent_date from stock) then
		return false;
elsif exists (select * from stock where rent_album.rent_date > stock.rent_date and mail = customer_mail) or exists (select * from stock where return_date is not null) then
return false;
else
insert into stock values (default, customer_mail, album, rent_date, default);
update album
set album_stock = album_stock - 1 where rent_album.album = album_name;
return true;
end if;
exception when others then
return false;
end;
$$ language plpgsql;

create or replace function return_album(CUSTOMER_MAIL VARCHAR(64), album varchar(64),
		return_date DTAE)
returns boolean as
$$
declare
   bum varchar(64);
   loan_date date;
begin
   bum := album;
   if album in (select album_name from album)
      or customer_mail in (select mail from customer)
      or exists (select * from stock where bum = stock.album_stock_name
	and stock.return_date is null and mail = customer_mail) then
	  select stock.rent_date into loan_date from stock
		where album_stock_name = album and mail = customer_mail
		and return_date is null;
	  if loan_date <= return_date then
		update album
		set album_stock = album_stock + 1 where album = album_name;
		update stock
		set stock.return_date = return_date and current_rent = current_rent + 1
		and total_rent = current_rent + 1;
		where mail = customer_mail and stock.album_stock_name = bum
		and stock.return_date is null;
		return true;
	  end if;
    end if;
exception when others then
  return false;
end;
$$ language plpgsql;

create or replace view view_customers as
select mail as email, name, total_rent as total_rents,
current_rent as current_rents from customer inner join stock on customer.mail = stock.mail;
group by mail order by mail;

create or replace view view_rents as
select mail, album_stock_name as album, rent_date as begin,
rent_date + interval '14 days' as deadline, return_date as end from stock
order by rent_date, mail, album_stock_name;

create or replace view view_stocks as
 select album_name as album, album_stock as stock from album
 order by album_name;
