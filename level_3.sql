create or replace function get_current_rents(STOCK_DATE DATE)
returns table(album varchar(64), mail varchar(64)) as
$$
begin 
     return query
	select mail, album_stock_name as album from stock where rent_date = STOCK_DATE
	order by album_stock_name, mail;
end
$$ language plpgsql;

create or replace function ALBUM_CONTENTS(ALBUM VARCHAR(64))
  returns table (N INT, TITLE VARCHAR(64), ARTIST varchar(64),
			DURATION varchar(16)) as
$$
begin 
     return query
	select song_id as n, songs_title as title, artist,
		duration_to_string(duration) as duration from song
	inner join album_song on album_song.songs_id = song.song_id
		where album_song.title = album order by song_id;
end
$$ language plpgsql;

create or replace function generate_bills(bill_year INT)
returns table(name varchar(64), mail varchar(64), price real) as
$$
begin 
	return query
  select name, mail, cast(sum(case when return_date - rent_date between 1 and 4 then 0.84
			      case when return_date - rent_date >= 5 then 
	
