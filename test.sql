\i src/init_database.sql
\i src/level_1.sql
\i src/level_2.sql

select duration_to_string(5);

select duration_to_string(213);

select duration_to_string(6008);

select duration_to_string(-3);

select add_artist('1995', '2008-08-27');

select add_artist('Nekfeu', '1990-04-03');

select add_artist('1995', '2013-05-14');

select add_artist('Nekfeu', '1994-10-23');

select add_album('Cyborg');

select add_album('Dans la légende');

select add_album('La source');

select add_album('LA SOURCE');

select add_album('Dans la légende');

select add_song('Nekketsu', 'Cyborg', 'Nekfeu', 2, 4*60+43);

select add_song('Humanoïde', 'Cyborg', 'Nekfeu', 2, 3*60+24);

select add_song('Nekketsu', 'La source', '1995', 14, 3*60+24);

select add_song('J''suis QLF', 'Dans la légende', 'PNL', 7, 4*60+20);

select add_song('Comme Un Grand', 'La suite', '1995', 2, 4*60+23);

select add_song('Milliardaire', 'Cyborg', 'Nekfeu', 3,4*60+24);

select add_song_to_album('Milliardaire', 'La source', 'Nekfeu', 16);

select add_song_to_album('Milliardaire', 'La source', 'Nekfeu', 15);

select * from view_artists;
select * from view_albums;
select * from view_songs;

select add_customer('Florent YOUINOU', 'youino_f@epita.fr');

select add_customer('Anas Haloui', 'youino_f@epita.fr');

select add_customer('Anas Haloui', 'haloui_a@epita.fr');

select add_customer('Abdel Hafidi', 'hafidi_a@epita.fr');

select add_stock('Cyborg', 5);

select add_stock('Unknown', 3);

select rent_album('haloui_a@epita.fr', 'Cyborg', '2017-03-04');

select rent_album('hafidi_a@epita.fr', 'Dans la légende', '2017-03-04');

select rent_album('haloui_a@epita.fr', 'Dans la légende', '2017-03-04');

select rent_album('haloui_a@epita.fr', 'Cyborg', '2017-03-05');

select rent_album('hafidi_a@epita.fr', 'Cyborg', '2017-03-16');

select return_album('haloui_a@epita.fr', 'Cyborg', '2017-03-14');

select return_album('hafidi_a@epita.fr', 'Dans la légende', '2017-03-15');

select return_album('haloui_a@epita.fr', 'Dans la légende', '2017-03-10');

select return_album('hafidi_a@epita.fr', 'Cyborg', '2017-04-05');

SELECT * FROM VIEW_CUSTOMERS;

SELECT * FROM VIEW_RENTS;

SELECT * FROM VIEW_STOCKS;
